This is a project to experience Angular for its data-driven design pattern.

It is an existing tabular data grid that has many records with pagination. Within some of the cells, there is additional information in response to UI events.

The idea is to load data thru Angular Resource, and bind to a template by Angular. And it should be pretty easy to implement some features like type ahead.

In fact it saves lots of time to create UI interactions, such as onChange, onClick to reload the table. And network transmission is limited to only a few time before
the page is loaded. However, creating custom filters on the data set requires quite amount of work. It eventually ran into performance issue pretty quick. Which
probably due to too many double-bindings within the chain of filtering. I tried to remove unnecessary double-bindings, instead using local variables as much as 
possible.

Eventually I kinda realized that Angular is probably not good for presenting large dataset in traditional tabular view, especially when its cells needs binding. 
It looks like limiting double-bindings is a must-do task to tweak performance of an Angular project.
