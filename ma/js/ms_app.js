// Your app's root module...
// fix http post data issue, from http://victorblog.com/2012/12/20/make-angularjs-http-service-behave-like-jquery-ajax/
window.msApp = angular.module('msApp', ['ui', 'ngResource'], function($httpProvider) {
  // Use x-www-form-urlencoded Content-Type
  $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

  // Override $http service's default transformRequest
  $httpProvider.defaults.transformRequest = [function(data)
  {
    /**
     * The workhorse; converts an object to x-www-form-urlencoded serialization.
     * @param {Object} obj
     * @return {String}
     */
    var param = function(obj)
    {
      var query = '';
      var name, value, fullSubName, subName, subValue, innerObj, i;

      for(name in obj)
      {
        value = obj[name];

        if(value instanceof Array)
        {
          for(i=0; i<value.length; ++i)
          {
            subValue = value[i];
            fullSubName = name + '[' + i + ']';
            innerObj = {};
            innerObj[fullSubName] = subValue;
            query += param(innerObj) + '&';
          }
        }
        else if(value instanceof Object)
        {
          for(subName in value)
          {
            subValue = value[subName];
            fullSubName = name + '[' + subName + ']';
            innerObj = {};
            innerObj[fullSubName] = subValue;
            query += param(innerObj) + '&';
          }
        }
        else if(value !== undefined && value !== null)
        {
          query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }
      }

      return query.length ? query.substr(0, query.length - 1) : query;
    };

    return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
  }];
});

//window.msApp = angular.module('msApp', ['ui', 'ngResource']);

msApp.controller('rfqCtrl', function($scope, $filter, $resource, $http) {
	$scope.rfqs = {};

	$scope.lookupEndDate = new Date();
	$scope.lookupSource	 = '2';

	var resetLookupParams = function() {
		$scope.currentLookupSource		= angular.copy($scope.lookupSource);
		$scope.currentLookupEndDate		= angular.copy($scope.lookupEndDate);
		$scope.currentLookupStartDate	= angular.copy($scope.lookupStartDate);
		$scope.currentPage				= 0;
	};
	resetLookupParams();

	var getLookupParams = function() {
		return {
			endDate : $filter('date')($scope.lookupEndDate, 'yyyy-MM-dd'),
			startDate : (!!Date.parse($scope.lookupStartDate)) ? $filter('date')($scope.lookupStartDate, 'yyyy-MM-dd') : '',
			source : $scope.lookupSource
		};
	};

	$scope.dataserv = $resource('/rfq_manager/rfqdata.php', getLookupParams());

	var setupPreferences = function(data) {
		if (!data.preferences) return null;

		$scope.currentPage				= 0;
		$scope.pageSize					= data.preferences.maxPerPage;
		$scope.defaultDateRange			= data.preferences.defaultDateRange;
		$scope.lookupStartDate			= new Date(data.preferences.startDate);
		$scope.currentLookupStartDate	= angular.copy($scope.lookupStartDate);
		$scope.serviceStatusOptions		= data.preferences.serviceStatuses;
		$scope.productTypeOptions		= data.preferences.productTypes;
		$scope.searchFieldOptions		= data.preferences.searchFields;
		$scope.showSubAgentFlag			= data.preferences.showSubAgent;
		$scope.userType					= data.preferences.userType;

		if (data.preferences.defaultRfqStatus) {
			$scope.searchCriteria.service_status = data.preferences.defaultRfqStatus;
		};

		if (data.preferences.defaultSearchField) {
			$scope.searchField = data.preferences.defaultSearchField;
		};


		$scope.searchCriteria.loc_detail.service_status = $scope.serviceStatusOptions[0];
		if (data.preferences.defaultServiceStatus) {
			for (var i = 0; i < $scope.serviceStatusOptions.length; i++) {
				if (angular.isArray($scope.serviceStatusOptions[i].value) &&
					$scope.serviceStatusOptions[i].value.compare(data.preferences.defaultServiceStatus)) {
					$scope.searchCriteria.loc_detail.service_status = $scope.serviceStatusOptions[i];
					break;
				}
			}
		};

		$scope.searchCriteria.loc_detail.product_type = $scope.productTypeOptions[0];
		if (data.preferences.defaultProductType) {
			for (var i = 0; i < $scope.productTypeOptions.length; i++) {
				if (angular.isArray($scope.productTypeOptions[i].value) &&
					$scope.productTypeOptions[i].value.compare(data.preferences.defaultProductType)) {
					$scope.searchCriteria.loc_detail.product_type = $scope.productTypeOptions[i];
					break;
				}
			}
		};
	};

	$scope.rfqs = $scope.dataserv.get(function(result) {
		setupPreferences(result);
	});

	$scope.searchCriteria = {};

	/*
	 * Generally following options are for select boxes. Their values
	 * are the key in data. Make sure thay match.
	 */
	$scope.rfqSourceOptions = [
  	    {'label' : 'All', 'value' : '1' },
        {'label' : 'Form', 'value' : '2' },
        {'label' : 'External', 'value' : '8' }
	];

	$scope.rfqStatusOptions = [
   	    {'label' : 'All', 'value' : ''},
        {'label' : 'Open', 'value' : 'open'},
        {'label' : 'Close', 'value' : 'close'}
	];
	$scope.searchCriteria.service_status = '';

   	$scope.searchCriteria.loc_detail = {};
	$scope.productTypeOptions = [
		{'label' : 'All', 'value' : ''}
	];
   	$scope.searchCriteria.loc_detail.product_type = $scope.productTypeOptions[0];;

	$scope.serviceStatusOptions = [
   	    {'label' : 'All', 'value' : ''}
	];
	$scope.searchCriteria.loc_detail.service_status = $scope.serviceStatusOptions[0];

   	$scope.searchCriteria.loc_detail.service_location = '';

	/*
	 * This is the complex case where search field is defined in one dropdown,
	 * and value in another, known as 'searchField' and 'searchValue'. For
	 * nested properties, use 'loc_detail.' as prefix. Make sure those fields
	 * exist in data.
	 */
   	$scope.searchFieldOptions = [];
	$scope.searchField = 'tracking_number';

	/*
	 * Angular ng-repeat doesn't honor object key order in object.
	 * Instead it re-order all keys in probably alphabetic order.
	 * following is the way to enforce it.
	 */
	$scope.natualOrder = function(obj) {
		if (!obj) return [];
		return Object.keys(obj);
	};

	var maskedSortColumns = ['qs', 'doc_sent', 'product_type', 'service_location', 'service_status'];
	$scope.isMasked = function(field) {
		return $.inArray(field, maskedSortColumns) >= 0;
	};

	$scope.sortField = 'initiated';
	$scope.sortDesc = true;
	$scope.setSortKey = function(field) {
		if ($scope.isMasked(field))
			return false;

		if (field == $scope.sortField) {
			$scope.sortDesc = !$scope.sortDesc;
		} else {
			$scope.sortField = field;
			$scope.sortDesc	 = false;
		}
	};

	$scope.currentPage	= 0;
	$scope.pageSize		= 5;

	$scope.filtered = function() {
		return $filter('dynamicFilter')(
				$scope.rfqs.data,
				[$scope.searchField, $scope.searchValue, $scope.searchCriteria]
			);
	};

	$scope.numberOfPages = function() {
		var l = (!$scope.filtered()) ?
			(($scope.rfqs.data) ? $scope.rfqs.data.length : 0)
			:
			$scope.filtered().length;
		return Math.ceil(l / $scope.pageSize);
	};

	getLocationLines = function(dataset) {
		var _num = 0;

		if (angular.isArray(dataset)) {
			$.each(dataset, function(key, value) {
				_num += (value.loc_detail) ? value.loc_detail.length : 0;
			});
		}

		return _num;
	};

	$scope.sizeOfData = function() {
		return getLocationLines($scope.rfqs.data);
	};

	$scope.numberOfLocations = function() {
		var _dataSet = ($scope.filtered()) ? $scope.filtered() : $scope.rfqs.data;
		return getLocationLines(_dataSet);
	};

	$scope.numberOfFiltered = function() {
		return ($scope.filtered()) ? $scope.filtered().length : 0;
	};

	$scope.setPage = function(num) {
		$scope.currentPage += num;
	};

	$scope.syncModels = function() {
		$scope.currentPage = 0;
		if ($scope.searchField.indexOf('.') != -1) {
			var s = $scope.searchField.split('.');
			if ($scope.searchCriteria[s[0]]) {
				$scope.searchCriteria[s[0]][s[1]] = $scope.searchValue;
			}
		}
	};

	$scope.loadMore = function() {
		var _params = getLookupParams();
		if ($scope.showLoadMoreButton()) {
			if ($scope.searchValue) {
				_params[$scope.searchField] = $scope.searchValue;
			}
		}

		$scope.rfqs = $scope.dataserv.get(_params);

		resetLookupParams(); // reset for each load
	};

	$scope.showLoadMoreButton = function() {
		/*
		 * Right now, it's interested in following conditions:
		 * 1, When search against RFQ id or Quote id, and can't find any in existing dataset;
		 * 2, When search date range is changed, and both inputs are valid;
		 * 3, Submission source is changed.
		 * Above will show the 'load more' button and trigger loading/refreshing dataset.
		 */
		if (('tracking_number' == $scope.searchField && angular.isArray($scope.filtered()) && $scope.filtered().length == 0) ||
			(!!Date.parse($scope.lookupStartDate) &&
			 !!Date.parse($scope.lookupEndDate) &&
			 (!angular.equals($scope.currentLookupEndDate, $scope.lookupEndDate) ||
			  !angular.equals($scope.currentLookupStartDate, $scope.lookupStartDate))) ||
			($scope.currentLookupSource != $scope.lookupSource) ||
			('quote_sq_id' == $scope.searchField && $scope.searchValue)) {

			return true;
		}

		return false;
	};

	$scope.showRow = function(showsub, sellthru, data, key) {
		if (showsub && stripTags(sellthru) && $.inArray(key, ['initiated', 'product_type', 'tracking_number']) == -1) {
			return stripTags(data[key]);
		}

		return data[key];
	};

	$scope.exportRfq = function() {
		var exportParams = {
			"search_assignee_id"    : $scope.searchCriteria.search_assignee_id, // not exist yet
			"search_rfq_status"     : $scope.searchCriteria.service_status,
			"search_service_status" : $scope.searchCriteria.loc_detail.service_status,
			"search_product_type"   : $scope.searchCriteria.loc_detail.product_type,
			"search_field"          : $scope.searchField,
			"search_value"          : $scope.searchValue,
			"searchDateFrom"        : $filter('date')($scope.lookupStartDate, 'yyyy-MM-dd'),
			"searchDateTo"          : $filter('date')($scope.lookupEndDate, 'yyyy-MM-dd'),
			"search_source"         : $scope.lookupSource
		};

//		console.log($.param(exportParams));
		window.location = "/ma/rfq_manager_sq/export_rfqs.php?" + $.param(exportParams);
	};

	$scope.memorize = function() {
		var memorizeParams = {
			"search_assignee_id"    : $scope.searchCriteria.search_assignee_id, // not exist yet
			"search_rfq_status"     : $scope.searchCriteria.service_status,
			"search_service_status" : $scope.searchCriteria.loc_detail.service_status.value,
			"search_product_type"   : $scope.searchCriteria.loc_detail.product_type.value,
			"search_field"          : $scope.searchField,
			"search_value"          : $scope.searchValue,
			"default_date_range"	: $scope.defaultDateRange,
			"max_record_per_page"	: $scope.pageSize,

			"search_source"         : $scope.lookupSource
		};

		$http({
		    url:		"/ma/rfq_manager_sq/memorize.php",
		    method:		"POST",
		    data:		memorizeParams
		}).success(function(data, status, headers, config) {
		    alert('Your search preferences are saved.');
		}).error(function(data, status, headers, config) {
		    alert('Oops, you search preferences are not saved.');
		});
	};

});

msApp.filter('orderByStripped', function() {
	return function(input, field, dir) {
		if (!input) return [];

		// 'initiated' has an auxilary field holding real sql data of datetime
		field = ('initiated' == field) ? field + '_mod' : field;

		input.sort(function(a, b) {
			var stripped_a = stripTags(a[field]);
			var stripped_b = stripTags(b[field]);
			if (stripped_a == stripped_b)
				return 0;
			if (dir)
				return stripped_a > stripped_b ? -1 : 1;
			else
				return stripped_a > stripped_b ? 1 : -1;
		});

		return input;
	};
});

msApp.filter('startFrom', function() {
    return function(input, start) {
    	if (!input) return [];
    	return input.slice(+start); // +start, parse to int
    };
});

msApp.filter('dynamicFilter', function () {
    return function (input, keyValuePairs) {
    	if (!input) return [];

        var obj = {}, i;
        for (i = 0; i < keyValuePairs.length; i += 2) {
        	if (angular.isObject(keyValuePairs[i])) {
        		angular.extend(obj, keyValuePairs[i]);
        	} else if (keyValuePairs[i] && !!keyValuePairs[i+1]) {
        		var splitted = keyValuePairs[i].split('.');
        		if (splitted.length > 1) {
        			obj[splitted[0]] = obj[splitted[0]] || {};
        			obj[splitted[0]][splitted[1]] = keyValuePairs[i+1];
        		} else {
        			obj[splitted[0]] = keyValuePairs[i+1];
        		}
            }
        }
        obj = scrubFilterValues(obj) || {};
        //return $filter('filter')(input, obj);

        var deepmap = function(record, filterObj) {
			var flag = true;

			$.each(filterObj, function(key, value) {
				if ($.isPlainObject(value)) {
					if (!$.isArray(record[key])) flag = false;

					record[key + '_mod'] = $.map(record[key], function(_val, _idx) {
						return deepmap(_val, value);
					}) || [];

					flag = flag && !!record[key + '_mod'].length;
				} else if ($.isArray(value)) { // handles array case, eg. service status 'All Finished'
					var _stripped = stripTags(record[key]);
					var _j_found = false;
					for (var j = 0; j < value.length; j++) {
						if (value[j].toLowerCase() == _stripped.toLowerCase()) {
							_j_found = true;
							break;
						}
					}

					flag = flag && _j_found;
				} else if (!!value) {
					if (angular.isString(record[key])) {
						var _stripped = stripTags(record[key]);
						flag = flag && (_stripped.toLowerCase().indexOf(value.toLowerCase()) != -1);
					}
				}
			});

        	return flag ? record : null;
        };

        return $.map(input, function(val, idx) {
        	return deepmap(val, obj);
        });
    };

});

msApp.directive('highlight', function() {
    return function(scope, element) {
        element.bind('mouseenter', function(){
        	element.addClass('orangeBg7');
        }).bind('mouseleave', function(){
        	element.removeClass('orangeBg7');
        });
    };
});

msApp.directive('disableKey', function() {
	return function(scope, element) {
		/*
		 * I meant to disable all key event here to prevent user from entering
		 * anything by typing. But 'keypress' doesn't catch Backspace, 'keyup'
		 * doesn't work. Only 'keydown' works here, not sure exactly why yet.
		 * But in case it doesn't work due to vendor package upgrade, or
		 * anything, do try out above and other key events. -syang
		 */
		element.bind('keydown', function(event) {
			event.preventDefault();
		});
	};
});

/*
 * To compile the content in HTML by Angular, unfortunately not working in
 * my case yet.
 *
 * Example: <li ng-repeat="r in data" htmlinsert="r.alink"></li>
 */
msApp.directive('htmlinsert',function($compile){
    return {
        scope: {
            htmlinsert: '='
        },
        link: function(scope,element,attrs) {
        	console.log(scope.htmlinsert);
            var compiledElement = $compile(scope.htmlinsert)(scope);
            element.append(compiledElement);
        }
    };
});

/*
 * Following are copied from /inc/javascripts/rfqManager.js
 * Sorry, I have no way to make it work with angular directly. The problem was:
 * 1, I can not load rfqManager.js, because it has .live() which is removed in
 * jquery 1.9.x. And angular needs new jquery to work;
 * 2, Theoratically, I could compile cell html in angular to make it available
 * to jQuery, so $('a.getRequirements') could work, but the markup contains too
 * many inline html code, which fails angular compile process.
 *
 */
function callGetReq(reqid) {

	dialogObj = $('<div></div>')
	.load('/ma/rfq_manager_sq/view_requirements.php?reqsqidxid='+reqid,
		{},
		function(responseText) {
			dialogObj.dialog({
				title: false,
				modal: true,
				width: 550
			});

			$("div.ui-dialog-titlebar").css('background','transparent').css('border','0').css('float','right');
		}
	);

}

function callGetIlec(npanxx) {
	//var npanxx = $(this).data('npanxx');
	dialogObj = $('<div></div>')
	.load('/resources/wire_center.html?dialog=1&npanxx='+npanxx,
		{},
		function(responseText) {
			dialogObj.dialog({
				title: false,
				modal: true,
				width: 850
			});
			$(".ui-dialog-titlebar").hide();

			// closes ilec
			$('a.dialogCloser').on('click',function(){
				if (dialogObj != null) {
					dialogObj.dialog('close');
				}
			});
		}
	);
}


/*
 * Following are utility function
 */
function stripTags(str) {
	return str.replace(/<[^br\s*]\/?\s*\w.*?>/ig, '');
}

function scrubFilterValues(obj) {
	if (isObject(obj)) {
		var _tmp = {};
		for (var key in obj) {
			var value = obj[key];

			/*
			 * In case that I have SELECT option value as array, I can't set
			 * the model value to array to change the selected option. It seems
			 * Angular only compares primitive types like string.
			 * The workaround here is arkward:
			 * I have to use $scope.field = $scope.options[2] format to set
			 * selected value, compare to $scope.field = 'All'
			 * And in template, instead of using:
			 * o.value as o.label for o in serviceStatusOptions
			 * I have to use:
			 * o.label for o in serviceStatusOptions
			 * the before case sets o.label to the model; While the after case
			 * sets the entire option object to the model, for example:
			 * {label : 'All Finished', value : ['Ready', 'Requested Docs', ..]}
			 * And then, I'll have to acknowledge this change when I compose
			 * the filter object here.
			 */
			value = (value && value.hasOwnProperty('value')) ? value.value : value;

			if (isObject(value)) {
				var child = scrubFilterValues(value);
				if (child) {
					_tmp[key] = child;
				}
			} else if (!!value) {
				_tmp[key] = value;
			}
		}
		return _tmp;
	} else {
		return false;
	}
}

function isObject(a) {
    return (!!a) && (a.constructor === Object);
};

function isArray(a) {
    return (!!a) && (a.constructor === Array);
};

//attach the .compare method to Array's prototype to call it on any array
Array.prototype.compare = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time
    if (this.length != array.length)
        return false;

    for (var i = 0; i < this.length; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].compare(array[i]))
                return false;
        }
        else if (this[i] != array[i]) {
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;
        }
    }
    return true;
};